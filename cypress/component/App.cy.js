import React from "react";
import App from '../../src/app';

describe('App Tests', () => {

  it('should render basic app', () => {
    cy.mount(<App />);
    cy.get('div').contains('my site')
  })

  it('should render passed in text', () => {
    cy.mount(<App text="my home"/>);
    cy.get('div').contains('my home')
  });

  it('should render nope', () => {
    cy.mount(<App text="my test"/>);

    cy.get('div').contains('test')
  });

})